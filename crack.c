#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
#include "md5.h"
 
const int PASS_LEN = 20;        // Maximum any password will be
const int HASH_LEN = 33;        // Length of MD5 hash strings
 
// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char * targetHash, char * dictionaryFilename)
{
    // Open the dictionary file
    FILE *d = fopen(dictionaryFilename,"r");
    if(!d)
    {
        char err[100];
        sprintf(err,"Can't open %s for reading",dictionaryFilename);
        perror(err);
        exit(1);
    }
    // Loop through the dictionary file, one line
    // at a time.
    char *word = malloc(100*sizeof(char *));
    while(fgets(word,100,d) != NULL)
    {
        char *nl = strchr(word,'\n');
        if(nl)
        {
            *nl = '\0';
        }
        // Hash each password. Compare to the target hash.
        char *hash = md5(word,strlen(word));
        if(strcmp(hash, targetHash) == 0)
        {
            // If they match, return the corresponding password.
            // Close the dictionary file. Free up memory?
            fclose(d);
            free(hash);
            return word;
        }    
       free(hash);   
    }
    fclose(d);
    return NULL;
}
 
 
int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        fprintf(stderr, "Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
 
    // Open the hash file for reading.
    FILE *h = fopen(argv[1], "r");
        if(!h)
    {
        char err[100];
        sprintf(err,"Can't open %s for reading",argv[1]);
        perror(err);
        exit(1);
    }
     char word[100];
    while(fgets(word,100,h) != NULL)
    {
        char *nl = strchr(word,'\n');
        if(nl)
        {
            *nl = '\0';
        }
        // For each hash, crack it by passing it to crackHash
        char *c = crackHash(word, argv[2]);
        // Display the hash along with the cracked password:
        //   5d41402abc4b2a76b9719d911017c592 hello
        printf("The hash is: %s, the cracked password is: %s\n",word, c);
        free(c);
    }
    fclose(h);
    // Close the hash file  
    // Free up any malloc'd memory?
    exit(1);
}
 

